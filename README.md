# OpenML dataset: QSAR-TID-36

https://www.openml.org/d/3820

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Dr Ivan Olier, Dr Jeremy Besnard, Dr Noureddin Sadawi, Dr Larisa Soldatova, Dr Crina Grosan, Prof Ross King, Dr Richard Bickerton, Prof Andrew Hopkins and Dr Willem van Hoorn  
**Source**: MetaQSAR project - September 2015  
**Please cite**:   

This dataset contains QSAR data (from ChEMBL version 17) showing activity values (unit is pseudo-pCI50) of several compounds on drug target TID: 36, and it has 1731 rows and 1026 features (including IDs and class feature: MOLECULE_CHEMBL_ID and MEDIAN_PXC50). The features represent FCFP 1024bit Molecular Fingerprints which were generated from SMILES strings. They were obtained using the Pipeline Pilot program, Dassault Systèmes BIOVIA. Generating Fingerprints does not usually require missing value imputation as all bits are generated.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/3820) of an [OpenML dataset](https://www.openml.org/d/3820). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/3820/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/3820/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/3820/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

